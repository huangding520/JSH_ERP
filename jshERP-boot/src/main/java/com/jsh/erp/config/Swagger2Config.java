package com.jsh.erp.config;

import com.jsh.erp.common.BaseSwaggerConfig;
import com.jsh.erp.common.SwaggerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author jishenghua
 * @version 1.0
 */
@Configuration
@EnableSwagger2
@Profile({"dev"})
public class Swagger2Config extends BaseSwaggerConfig {

    @Override
    public SwaggerProperties swaggerProperties() {
        return SwaggerProperties.builder()
            .apiBasePackage("com.jsh.erp.business")
            .title("项目骨架")
            .description("ERP相关接口文档")
            .contactName("system")
            .version("1.0")
            .enableSecurity(true)
            .build();
    }

}
