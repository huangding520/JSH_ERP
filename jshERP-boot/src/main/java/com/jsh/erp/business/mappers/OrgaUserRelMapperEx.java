package com.jsh.erp.business.mappers;

import com.jsh.erp.business.entities.OrgaUserRel; /**
 * Description
 *
 * @Author: cjl
 * @Date: 2019/3/12 9:13
 */
public interface OrgaUserRelMapperEx {

    int addOrgaUserRel(OrgaUserRel orgaUserRel);

    int updateOrgaUserRel(OrgaUserRel orgaUserRel);
}
